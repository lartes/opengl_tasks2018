#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "Maze.h"
#include <glm/glm.hpp>
#include "Camera.hpp"
#include <Texture.hpp>
#include <cstdlib>

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class TextureSet {
public:
    TexturePtr color;
    TexturePtr normal;

    TextureSet() {}
    TextureSet(TexturePtr color, TexturePtr normal) : color(color), normal(normal) {}
    TextureSet(char* color, char* normal) : color(loadTexture(color)), normal(loadTexture(normal)) {}
    
    TexturePtr operator[](int index) {
        if (index == 1) {
            return normal;
        }
        return color;
    }
};

class MazeApp : public Application {
public:
    vector<MeshPtr> meshes;
    vector<CameraMoverPtr> movers = {std::make_shared<OrbitCameraMover>(), std::make_shared<FreeCameraMover>()};
    int currentIndex = 0;
    int spotlight = 0;
    glm::vec2 pos;
    glm::vec2 vel;
    Maze maze;
    TextureSet bricks_texture;
    TextureSet floor_texture;
    vector<GLuint> samplers;

    ShaderProgramPtr shader;

    MazeApp() : maze(10),
        samplers(2),
        pos(0.0f, 0.0f),
        vel(0.0f, 0.0f),
        Application(nullptr) {
            _cameraMover = movers[0];
    }

    void makeScene() override
    {
        Application::makeScene();

        maze.generate();
        maze.debug();
        meshes = maze.get_meshes();
        meshes.push_back(makeFloor(0));
        for (MeshPtr mesh : meshes) {
            mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
        }

        //=========================================================

        shader = std::make_shared<ShaderProgram>("492ZinovData/spotlight.vert", "492ZinovData/spotlight.frag");
        bricks_texture = TextureSet("492ZinovData/bricks.jpg", "492ZinovData/bricks_norm.jpg");
        floor_texture = TextureSet("492ZinovData/floor.jpg", "492ZinovData/floor_norm.jpg");
        for (int i = 0; i < 2; ++i) {
            glGenSamplers(1, &(samplers[i]));
            glSamplerParameteri(samplers[i], GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glSamplerParameteri(samplers[i], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glSamplerParameteri(samplers[i], GL_TEXTURE_WRAP_S, GL_REPEAT);
            glSamplerParameteri(samplers[i], GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
    }

    void checkUpdate() override
    {
        if (currentIndex == 1) {
            maze.clamp_pos(std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos);
        }
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Maze", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::RadioButton("orbit camera", &currentIndex, 0);
            ImGui::RadioButton("first person camera", &currentIndex, 1);
            ImGui::RadioButton("overhead light", &spotlight, 0);
            ImGui::RadioButton("spotlight", &spotlight, 1);
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();
        Application::setCameraMover(movers[currentIndex]);

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        if (spotlight) {
            shader->setVec3Uniform("light.pos", std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos);
            glm::vec3 light_dir = glm::normalize(glm::vec3(0.0f, 0.0f, -1.0f) * std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_rot);
            glm::vec3 up = glm::normalize(glm::cross(light_dir, glm::vec3(1.0f, -1.0f, 1.0f)));
            glm::vec3 right = glm::normalize(glm::cross(light_dir, up));
            pos += vel;
            vel -= pos * 0.005f;
            vel -= vel * 0.01f;
            vel += glm::vec2((rand() * 1.0 / RAND_MAX - 0.5), (rand() * 1.0 / RAND_MAX - 0.5)) * 0.05f;
            //pos = glm::vec2(10.0f, 0.0f);
            shader->setVec3Uniform("light.coneDirection", light_dir + up * pos[0] * 0.01f + right * pos[1] * 0.01f);
            shader->setFloatUniform("light.coneAngle", 30.0f);
            shader->setVec3Uniform("light.Ld", glm::vec3(1.0f, 1.0f, 1.0f));
        } else {
            shader->setVec3Uniform("light.pos", glm::vec3(5.0f, 5.0f, 20.0f));
            shader->setVec3Uniform("light.coneDirection", glm::vec3(0.0f, 0.0f, -1.0f));
            shader->setFloatUniform("light.coneAngle", 180.0f);
            shader->setVec3Uniform("light.Ld", glm::vec3(100.0f, 100.0f, 100.0f));
        }
        shader->setVec3Uniform("light.La", glm::vec3(0.1f, 0.1f, 0.1f));

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (MeshPtr mesh : meshes) {
            auto texture_set = &bricks_texture;
            if (mesh->getMaterial()) {
                texture_set = &floor_texture;
            }
            for (int i = 0; i < 2; ++i) {
                if (USE_DSA) {
                    glBindTextureUnit(i, (*texture_set)[i]->texture());
                    glBindSampler(i, samplers[i]);
                }
                else {
                    glBindSampler(i, samplers[i]);
                    glActiveTexture(GL_TEXTURE0 + i);  //текстурный юнит 0
                    (*texture_set)[i]->bind();
                }
            }
            shader->setIntUniform("diffuseTex", 0);
            shader->setIntUniform("normalTex", 1);
            shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));

            shader->setVec3Uniform("material.Ka", glm::vec3(1.0f, 0.0f, 0.0f));
            shader->setVec3Uniform("material.Kd", glm::vec3(1.0f, 0.0f, 0.0f));
            mesh->draw();
        }
        glBindSampler(0, 0);
    }
};

int main()
{
    MazeApp app;
    app.start();
    app.maze.debug();

    return 0;
}
