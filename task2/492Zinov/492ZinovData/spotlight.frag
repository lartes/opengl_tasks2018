#version 330
/**
  Повершинное освещение точечным источником света. Только окружающий и диффузный света
 */


uniform sampler2D diffuseTex;
uniform sampler2D normalTex;
struct LightInfo
{
    vec3 pos; //положение источника света в мировой системе координат (для точечного источника)
    float coneAngle;
    vec3 coneDirection;
    vec3 La; //коэффициент отражения окружающего света
    vec3 Ld; //коэффициент отражения диффузного света
};
uniform LightInfo light;
uniform mat4 viewMatrix;
uniform mat3 normalToCameraMatrix;

struct MaterialInfo
{
    vec3 Ka; //коэффициент отражения окружающего света
    vec3 Kd; //коэффициент отражения диффузного света
};
uniform MaterialInfo material;

in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec3 normal;
in vec4 lightPosCamSpace;
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec3 fragColor; //выходной цвет фрагмента

void main()
{
    vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
    vec3 normalShift = texture(normalTex, texCoord).rgb;
    vec3 normalCamSpace = (normalToCameraMatrix * (normal + normalShift)); //преобразование нормали в систему координат камеры
    vec4 lightDirCamSpace = normalize(lightPosCamSpace - posCamSpace);
    float attenuation = 0.0;

    float lightToSurfaceAngle = degrees(acos(dot(-lightDirCamSpace.xyz, normalize(normalToCameraMatrix * light.coneDirection))));
    if(lightToSurfaceAngle < light.coneAngle){
        attenuation = cos(lightToSurfaceAngle / light.coneAngle * 3.14 / 2);
    }

    float dist = distance(lightPosCamSpace, posCamSpace);
    attenuation /= dist * dist;

    float NdotL = abs(dot(normalCamSpace, -lightDirCamSpace.xyz)); //скалярное произведение (косинус)

    fragColor = light.La * diffuseColor + light.Ld * diffuseColor * NdotL * attenuation; //цвет вершины
    //fragColor = normalCamSpace;
}
