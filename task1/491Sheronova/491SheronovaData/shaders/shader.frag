#version 330 core
in vec3 Position;


out vec4 color;

uniform vec4 lineColor;


void main()
{
	if (lineColor.x == 0.0f)
		color = vec4(1- Position.y, 1- Position.y, 1 - Position.y, 1.0f);
	else
		color = lineColor;
}
