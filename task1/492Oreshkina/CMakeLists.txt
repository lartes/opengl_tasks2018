set(SRC_FILES
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/Surface.cpp
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/Application.cpp
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/Camera.cpp
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/Mesh.cpp
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/ShaderProgram.cpp
        )

set(HEADER_FILES
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/Application.hpp
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/Camera.hpp
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/Common.h
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/Mesh.hpp
        ${PROJECT_SOURCE_DIR}/task1/492Oreshkina/common/ShaderProgram.hpp
        )




MAKE_TASK(492Oreshkina 1 "${SRC_FILES}")
