#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>

class SampleApplication : public Application
{
public:
    MeshPtr _shell;
    float _N = 20.0;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();

        _shell = makeShell(_N);
        _shell->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _shader = std::make_shared<ShaderProgram>("493LomovData/shaderNormal.vert", "493LomovData/shader.frag");
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _shell->modelMatrix());
        _shell->draw();
    }

    void update() override
    {
        double dt = glfwGetTime() - _oldTime;

        const double speed = 2.;
        const double multiplier = 0.8;

        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS)
        {
            if (_N < 50)
            {
                _N *= 1.1;
                makeScene();
            }
        }
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS)
        {
            if (_N > 10)
            {
                _N *= 0.9;
                makeScene();
            }
        }
        Application::update();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}