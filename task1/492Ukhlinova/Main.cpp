#include "Application.h"
#include "Mesh.h"
#include "ShaderProgram.h"

#include <vector>
#include <iostream>

class SampleApplication : public Application 
{
public:
	MeshPtr _breatherSurface;
	MeshPtr _surface;

	ShaderProgramPtr _shader;

	unsigned N = 100;
	double b = 0.4;
	int polygonMode = GL_FILL;
	int drawAxes = 0;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<OrbitCameraMover>();

		//Создаем меш с Breather Surface
		_breatherSurface = makeBreatherSurface(b, N);
		_breatherSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		_surface = makeSurface();
		_surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		//Создаем шейдерную программу        
		_shader = std::make_shared<ShaderProgram>("492UkhlinovaData/shaderNormal.vert", "492UkhlinovaData/shader.frag");
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("Breather Surface", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			ImGui::RadioButton("filled", &polygonMode, GL_FILL);
			ImGui::RadioButton("grid", &polygonMode, GL_LINE);

			ImGui::Text("Axes:");

			ImGui::RadioButton("OFF", &drawAxes, 0);
			ImGui::RadioButton("ON", &drawAxes, 1);
		}
		ImGui::End();
	}

	void handleKey(int key, int scancode, int action, int mods) override
    {
		// Изменение детализации поверхности по кнопкам с клавиатуры:
		// При нажатии на + количество полигонов должно увеличиваться, при нажатии на - уменьшаться.

        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
        	if (key == GLFW_KEY_1) {
        		polygonMode = GL_FILL;
        	}
        	else if (key == GLFW_KEY_2) {
        		polygonMode = GL_LINE;
        	} 
	        else if (key == GLFW_KEY_MINUS) {
	        	if (N >= 20) { 
					N -= 10;
					_breatherSurface = makeBreatherSurface(b, N);
					_breatherSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
					draw();
				}
        	}
        	else if (key == GLFW_KEY_EQUAL) {
        		if (N <= 300){ 
					N += 10;
					_breatherSurface = makeBreatherSurface(b, N);
					_breatherSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
					draw();
				}
        	} else if (key == GLFW_KEY_1) {

        	}
        }
	}

	void draw() override
	{
		Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Режим заливки полигона
		glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

        //Подключаем шейдер	
		_shader->use();

		//Устанавливаем общие юниформ-переменные
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//Рисуем меш
		_shader->setMat4Uniform("modelMatrix", _breatherSurface->modelMatrix());
		_breatherSurface->draw();

		if (drawAxes) {
			_shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
			_surface->draw();
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
};

int main(int argc, char** argv)
{
	SampleApplication app;
	app.start();

    return 0;
}