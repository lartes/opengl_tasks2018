#include "Camera.hpp"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>


FreeCameraHandler::FreeCameraHandler() :
        CameraHandler(),
        pos_(5.0f, 0.0f, 2.5f) {
    rot_ = glm::toQuat(glm::lookAt(pos_, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

// Не могу обрабатывать перемещение здесь, потому что нет времени
void FreeCameraHandler::handleKey(GLFWwindow *window, int key, int scan_code, int action, int mods) {}

void FreeCameraHandler::handleMouseMove(GLFWwindow *window, double x_pos, double y_pos) {
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS) {
        double dx = x_pos - old_x_pos_;
        double dy = y_pos - old_y_pos_;

        glm::vec3 right_dir = glm::vec3(1.0f, 0.0f, 0.0f) * rot_;
        rot_ *= glm::angleAxis(static_cast<float>(dy * 0.005), right_dir);

        glm::vec3 up_dir(0.0f, 0.0f, 1.0f);
        rot_ *= glm::angleAxis(static_cast<float>(dx * 0.005), up_dir);
    }

    old_x_pos_ = x_pos;
    old_y_pos_ = y_pos;
}

void FreeCameraHandler::handleScroll(GLFWwindow *window, double x_offset, double y_offset) {
}

void FreeCameraHandler::update(GLFWwindow *window, double dt) {
    float speed = 1.0f;

    // Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forward_dir = glm::vec3(0.0f, 0.0f, -1.0f) * rot_;

    // Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 right_dir = glm::vec3(1.0f, 0.0f, 0.0f) * rot_;

    // Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        pos_ += forward_dir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        pos_ -= forward_dir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        pos_ -= right_dir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        pos_ += right_dir * speed * static_cast<float>(dt);
    }

    // Соединяем перемещение и поворот вместе
    camera_info_.view_matrix = glm::toMat4(-rot_) * glm::translate(-pos_);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    // Обновляем матрицу проекции на случай, если размеры окна изменились
    camera_info_.projection_matrix = glm::perspective(glm::radians(45.0f),
                                                      (float) width / height, 0.1f, 100.f);
}