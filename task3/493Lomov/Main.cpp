#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <LightInfo.hpp>
#include <Texture.hpp>

#include <iostream>

class SampleApplication : public Application
{
public:
    MeshPtr _shell;
    MeshPtr _mirror;
    MeshPtr _light;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _shader_normals;
    ShaderProgramPtr _shader_mirror;
    ShaderProgramPtr _shader_light;

    float _N = 20;

    float degree_y = 0;
    float degree_z = 0;

    float _lr;
    float _phi;
    float _theta;
    LightInfo _light_info;

    GLuint _renderTexId;
    GLuint _framebufferId;
    TexturePtr _grassTexture;
    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;
    GLuint _sampler;
    CameraInfo _fbCamera;

    std::vector<glm::vec4> mirror_verts{{0, 0, 0, 1}, {0, 2, 0, 1}, {0, 0, 2, 1}, {0, 2, 2, 1}, {2, 2, 2, 1}};
    std::vector<glm::vec2> mirror_texs{{0, 0}, {1, 0}, {0, 1}, {1, 1}};

    void initFramebuffer() {
        //Создаем фреймбуфер
        glGenFramebuffers(1, &_framebufferId);
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        //Создаем текстуру, куда будет осуществляться рендеринг
        glGenTextures(1, &_renderTexId);
        glBindTexture(GL_TEXTURE_2D, _renderTexId);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
        glBindTexture(GL_TEXTURE_2D, 0);

        //Прикрепление текстуры к фреймбуфера
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _renderTexId, 0);

        //Создаем буфер глубины для фреймбуфера
        GLuint depthRenderBuffer;
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _fbWidth, _fbHeight);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        //Прикрепление буфера глубины к фреймбуфера
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);

        //Указываем куда именно мы будем рендерить
        GLenum buffers[] = {GL_COLOR_ATTACHMENT0};
        glDrawBuffers(1, buffers);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _shell = makeShell(_N);
        _shell->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _light = makeSphere(0.05f);
        _lr = 3.0;
        _phi = 0.0;
        _theta = glm::pi<float>() * 0.25f;
        _light_info.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        _light_info.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light_info.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light_info.specular = glm::vec3(1.0, 1.0, 1.0);

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("493LomovData/shader_light.vert", "493LomovData/shader_light.frag");
        _shader_normals = std::make_shared<ShaderProgram>("493LomovData/shader.vert", "493LomovData/shader.geom", "493LomovData/shader.frag");
        _shader_mirror = std::make_shared<ShaderProgram>("493LomovData/mirror.vert", "493LomovData/mirror.frag");
        _shader_light = std::make_shared<ShaderProgram>("493LomovData/light.vert", "493LomovData/light.frag");

        //Загрузка и создание текстур
        _grassTexture = loadTexture("493LomovData/sand.jpg");

        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        //Инициализация фреймбуфера
        initFramebuffer();
    }

    void drawToFramebuffer(const CameraInfo& camera)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        glViewport(0, 0, _fbWidth, _fbHeight);

        glClearColor(0.05, 0.05, 0.05, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

        _light_info.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light_info.position, 1.0));
        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light_info.ambient);
        _shader->setVec3Uniform("light.Ld", _light_info.diffuse);
        _shader->setVec3Uniform("light.Ls", _light_info.specular);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _grassTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        {
            _shader->setMat4Uniform("modelMatrix", _shell->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _shell->modelMatrix()))));
            _shell->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);

        //Отсоединяем фреймбуфер, чтобы теперь рендерить на экран
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void draw() override
    {
        Application::draw();

        //Создадим зеркало и повернем его
        _mirror = makeMirror();
        glm::mat4 tr = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, 0.0f, 0.0f));
        glm::mat4 rt = glm::rotate(glm::mat4(1.0), degree_y, glm::vec3(0, 1, 0));
        rt = glm::rotate(rt, degree_z, glm::vec3(0, 0, 1));
        _mirror->setModelMatrix(tr * rt);

        //Координаты зеркала
        glm::mat4 translation = _camera.viewMatrix * _mirror->modelMatrix();
        std::vector<glm::vec3> mirror_verts_3;
        for (int i = 0; i < mirror_verts.size(); ++i){
            glm::vec4 result_4 = translation * mirror_verts[i];
            glm::vec3 result_3(result_4 / result_4[3]);
            mirror_verts_3.push_back(result_3);
        }

        //Расстояния
        glm::vec3 normal = glm::normalize(mirror_verts_3[4] - mirror_verts_3[3]);
        float distance = glm::dot(normal, mirror_verts_3[3]);
        float move_dist = 2 * distance;
        glm::vec3 translate_camera = move_dist * normal;
        glm::vec3 point_dir{0, 0, -1};
        float dist_to_point = distance / glm::dot(point_dir, normal);
        glm::vec3 point = point_dir * dist_to_point;

        //Направление для виртуальной камеры
        glm::mat4 inverse = glm::inverse(_camera.viewMatrix);
        glm::vec3 new_cam_world_coord = inverse * glm::vec4{translate_camera, 1};
        glm::vec3 direction_for_imaginary_camera = inverse * glm::vec4{0, 0, 0, 1};

        //Задание матриц для камеры
        _fbCamera.viewMatrix = glm::lookAt(new_cam_world_coord, direction_for_imaginary_camera, glm::vec3(0.0f, 0.0f, 1.0f));

        glm::mat4 mv = _fbCamera.viewMatrix * _mirror->modelMatrix();
        glm::vec3 leftBottom = glm::vec3(mv * glm::vec4(mirror_verts[0]));
        glm::vec3 rightTop = glm::vec3(mv * glm::vec4(mirror_verts[3]));
        float near = glm::abs(leftBottom.z);
        float far = 30;
        _fbCamera.projMatrix = glm::frustum(leftBottom.x, rightTop.x, leftBottom.y, rightTop.y, near, far);

        //Рисуем на зеркале
        drawToFramebuffer(_fbCamera);



        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClearColor(0, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



        _shader_normals->use();

        //Устанавливаем общие юниформ-переменные
        _shader_normals->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader_normals->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader_normals->setMat4Uniform("modelMatrix", _shell->modelMatrix());
        _shell->draw();



        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light_info.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light_info.position, 1.0));
        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light_info.ambient);
        _shader->setVec3Uniform("light.Ld", _light_info.diffuse);
        _shader->setVec3Uniform("light.Ls", _light_info.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _grassTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        _shader->setMat4Uniform("modelMatrix", _shell->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _shell->modelMatrix()))));
        //_shell->draw();



        //Зеркало
        _shader_mirror->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader_mirror->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader_mirror->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        glBindTexture(GL_TEXTURE_2D, _renderTexId);
        _shader_mirror->setIntUniform("diffuseTex", 0);

        _shader_mirror->setMat4Uniform("modelMatrix", _mirror->modelMatrix());
        _mirror->draw();



        _shader_light->use();

        _shader_light->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light_info.position));
        _shader_light->setVec4Uniform("color", glm::vec4(_light_info.diffuse, 1.0f));
        _light->draw();

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light_info.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light_info.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light_info.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void update() override
    {
        double dt = glfwGetTime() - _oldTime;

        const double speed = 2.;
        const double multiplier = 0.8;
        if (glfwGetKey(_window, GLFW_KEY_UP) == GLFW_PRESS)
        {
            degree_y += speed * dt;
        }
        if (glfwGetKey(_window, GLFW_KEY_DOWN) == GLFW_PRESS)
        {
            degree_y -= speed * dt;
        }
        if (glfwGetKey(_window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        {
            degree_z += speed * dt;
        }
        if (glfwGetKey(_window, GLFW_KEY_LEFT) == GLFW_PRESS)
        {
            degree_z -= speed * dt;
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS)
        {
            if (_N < 50)
            {
                _N *= 1.1;
                makeScene();
            }
        }
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS)
        {
            if (_N > 10)
            {
                _N *= 0.9;
                makeScene();
            }
        }
        Application::update();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}