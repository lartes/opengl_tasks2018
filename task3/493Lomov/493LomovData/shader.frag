/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330

in VS_OUT {
    vec3 normal;
    vec4 vColor;
} gs_in;

out vec4 fragColor;

void main()
{
    fragColor = gs_in.vColor;
}
