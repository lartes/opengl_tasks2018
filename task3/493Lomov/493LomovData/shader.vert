/*
Простейший вершинный шейдер для первого семинара. Подробности - в семинаре №2
*/

#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out VS_OUT {
    vec3 normal;
    vec4 vColor;
} vs_out;

void main()
{
    /*
    Меш не имеет атрибута цвет (диапазон [0; 1]), но имеет атрибут нормаль (диапазон [-1; 1]).
    Нормаль легко перевести в цвет для отладочных и демонстрационных задач.
    */
    vs_out.vColor.rgb = normalize(vertexNormal);
    vs_out.vColor.a = 1.0;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 5.0);
    mat3 normalMatrix = mat3(transpose(inverse(viewMatrix * modelMatrix)));
    vs_out.normal = normalize(vec3(projectionMatrix * vec4(normalMatrix * vertexNormal, 0.0)));
}
