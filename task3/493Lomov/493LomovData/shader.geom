
#version 330

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

out vec4 fColor;

in VS_OUT {
    vec3 normal;
    vec4 vColor;
} gs_in[];

const float MAGNITUDE = 2;

void GenerateLine(int index)
{
    fColor = gs_in[index].vColor;
    gl_Position = gl_in[index].gl_Position;
    EmitVertex();
    gl_Position = gl_in[index].gl_Position + vec4(gs_in[index].normal, 0.0) * MAGNITUDE;
    EmitVertex();
    EndPrimitive();
}

void main()
{
    GenerateLine(0); // вектор нормали для первой вершины
    GenerateLine(1); // ... для второй
    GenerateLine(2); // ... для третьей
}